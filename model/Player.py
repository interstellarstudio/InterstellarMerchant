from typing import Optional

from model.Planet import Planet
from model.Ship import Ship
from model.SolarSystem import SolarSystem


class Player:
    """
    This class represents the player
    """
    def __init__(self,
                 player_name: str,
                 ship: Optional[Ship],
                 solar_system: SolarSystem,
                 planet_location: Optional[Planet],
                 _credits: int = 0):
        self._name: str = player_name
        self._ship: Ship = ship
        self._credits: int = _credits
        self._system_location: SolarSystem = solar_system
        self._planet_location: Optional[Planet] = planet_location

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, new_name: str) -> None:
        if not new_name:
            return
        self._name = new_name

    @property
    def ship(self) -> Optional[Ship]:
        return self._ship

    @ship.setter
    def ship(self, new_ship: Optional[Ship]) -> None:
        self._ship = new_ship

    @property
    def credits(self) -> int:
        return self._credits

    @credits.setter
    def credits(self, new_credits: int) -> None:
        self._credits = new_credits

    def add_credits(self, amount: int) -> int:
        self._credits += amount
        return self._credits

    def remove_credits(self, amount: int) -> bool:
        if self._credits - amount <= 0:
            return False
        self._credits -= amount
        return True

    @property
    def system_location(self) -> SolarSystem:
        return self._system_location

    @property
    def planet_location(self) -> Planet:
        return self._planet_location

    def get_full_location(self) -> str:
        if not self._planet_location:
            return self._system_location.name
        return f"{self._system_location.name} - {self._planet_location.name}"

    def travel_to_system(self, destination: SolarSystem) -> bool:
        if destination.name == self._system_location.name:
            return False
        self._system_location = destination
        self._planet_location = None
        return True

    def travel_to_planet(self, destination: Planet) -> bool:
        if destination.name == self._planet_location.name:
            return False
        if destination not in self._system_location.planets:
            return False
        self._planet_location = destination
        return True
