from random import randint
from typing import Optional

from model.Tradepost import TradePost, build_random_trade_post


class Planet:
    def __init__(self, name: str, trade_post: Optional[TradePost] = None, style: Optional[int] = None):
        self._name: str = name
        if not style:
            self._style: int = randint(0, 7)  # Choose a style at random and persist it
        else:
            self._style: int = style

        if not trade_post:
            self._trade_post = build_random_trade_post()
        else:
            self._trade_post = trade_post

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, new_name: str) -> None:
        self._name = new_name

    @property
    def style(self) -> int:
        return self._style

    @property
    def trade_post(self) -> TradePost:
        return self._trade_post
