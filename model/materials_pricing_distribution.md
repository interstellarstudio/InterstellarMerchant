# Pricing Distribution

This table is a reference document that lists the median prices for all materials.

The average stock for all wares is between 50 and 500.

|      Material      | Lowest Price | Highest Price | Tier |
|--------------------|--------------|---------------|------|
| MolecularNiobium   | 1            | 20            | 1    |
| LuminousSteel      | 13           | 34            | 1    |
| DarkChromium       | 7            | 42            | 1    |
| Graphene           | 71           | 212           | 2    |
| PhotonicTube       | 80           | 256           | 2    |
| Erythosium         | 52           | 301           | 3    |
| Axonanium          | 64           | 199           | 3    |
| Dextrorus          | 102          | 407           | 4    |
| Neptunesium        | 165          | 399           | 4    |
| Vibronium          | 210          | 480           | 5    |
| Seismoril          | 400          | 800           | 6    |
| Phosium            | 487          | 1090          | 6    |