from model.Inventory import DebugInventory
from model.Ship import Ship


class DebugShip(Ship):
    """
    The class DebugShip is meant to test features
    """
    def __init__(self):
        super().__init__("HMS Debugger", 1000, 1)
        # Cargo
        self._inventory = DebugInventory()

        # Crew
        self._max_crew_capacity: int = 2
        self._current_crew_capacity: int = 0

        # Armor
        self._max_shield_orbs: int = 3
        self._current_shield_orbs: int = 1
        self._can_equip_kinetic_dampeners: bool = True
        self._has_kinetic_dampeners_equipped: bool = False

        # Weapons
        self._max_laser_capacity: int = 6
        self._current_laser_capacity: int = 2

        # Test data
        self._current_hit_points = 875

