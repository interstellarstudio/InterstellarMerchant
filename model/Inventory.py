from copy import deepcopy
from typing import Type

from model.Materials import Material, MolecularNiobium, LuminousSteel, DarkChromium, \
    PhotonicTube, Graphene, Erythosium, Axonanium, Dextrorus, Neptunesium, Vibronium, Seismoril, Phosium


class Inventory:
    def __init__(self, max_cargo_capacity: int = 100):
        self._max_cargo_capacity: int = max_cargo_capacity
        self._internal_inventory: list[Material] = []

    @property
    def max_cargo_capacity(self) -> int:
        return self._max_cargo_capacity

    @max_cargo_capacity.setter
    def max_cargo_capacity(self, new_max: int) -> None:
        if new_max < 1:
            return
        self._max_cargo_capacity = new_max

    @property
    def current_cargo_capacity(self) -> int:
        return sum([mat.quantity for mat in self._internal_inventory])

    def increase_max_cargo_capacity(self, increase_amount: int) -> None:
        self._max_cargo_capacity += increase_amount

    # Note: Debug only method
    def list_materials(self) -> None:
        for material in self._internal_inventory:
            print(f"{material.name} - {material.quantity}")

    # ToDo: Check if adding would exceed max cargo capacity and return False if it does
    def add_material(self, material: Material) -> bool:
        for mat in self._internal_inventory:
            if isinstance(mat, type(material)):
                if (self.current_cargo_capacity + material.quantity) > self._max_cargo_capacity:
                    return False
                mat.quantity += material.quantity
                return True
        self._internal_inventory.append(material)
        return True

    def remove_material(self, material: Type[Material], quantity: int) -> bool:
        for mat in self._internal_inventory:
            if isinstance(mat, material) and mat.quantity > quantity:
                mat.quantity -= quantity
                return True
            elif isinstance(mat, material) and mat.quantity == quantity:
                self._internal_inventory.remove(mat)
                return True
        return False

    def get_inventory_list(self) -> list[Material]:
        return deepcopy(self._internal_inventory)


class DebugInventory(Inventory):
    def __init__(self):
        super().__init__(max_cargo_capacity=1234)
        self.add_material(MolecularNiobium(50))
        self.add_material(LuminousSteel(120))
        self.add_material(DarkChromium(34))
        self.add_material(Graphene(7))
        self.add_material(PhotonicTube(80))
        self.add_material(Erythosium(50))
        self.add_material(Axonanium(120))
        self.add_material(Dextrorus(34))
        self.add_material(Neptunesium(7))
        self.add_material(Vibronium(80))
        self.add_material(Seismoril(80))
        self.add_material(Phosium(80))

        # Stack test
        self.add_material(Vibronium(80))
        self.add_material(Vibronium(80))


# ToDo: Write Unittests
if __name__ == "__main__":
    inv = Inventory()
    mat1 = MolecularNiobium(5)
    mat2 = LuminousSteel(12)
    inv.add_material(mat1)
    inv.add_material(mat2)
    inv.list_materials()
    inv.remove_material(MolecularNiobium, 1)
    inv.list_materials()
    inv.remove_material(LuminousSteel, 12)
    inv.list_materials()
