from math import hypot
from random import randint
from typing import Optional

from model.Planet import Planet


class SolarSystem:
    def __init__(self, name: str, planets: list[Planet]):
        self._name: str = name
        self._planets: list[Planet] = planets
        self._map: list[list[Optional[Planet]]] = [[None for _ in range(6)] for _ in range(6)]
        self._distribute_planets()

    # ToDo: This way of distribution sometimes result in planets being clumped together.
    #       Develop a way to evenly, yet randomly, distribute the planets
    def _distribute_planets(self) -> None:
        def get_random_coordinates() -> tuple[int, int]:
            return randint(0, len(self._map) - 1), randint(0, len(self._map[0]) - 1)
        for planet in self._planets:
            new_random_coordinates = get_random_coordinates()
            while self._map[new_random_coordinates[0]][new_random_coordinates[1]]:  # Calculate random until free spot
                new_random_coordinates = get_random_coordinates()
            self._map[new_random_coordinates[0]][new_random_coordinates[1]] = planet

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, new_name: str) -> None:
        self._name = new_name

    @property
    def planets(self) -> list[Planet]:
        return self._planets

    @planets.setter
    def planets(self, new_planets: list[Planet]) -> None:
        self._planets = new_planets

    def get_coordinates_for_planet(self, planet: Planet) -> Optional[tuple[int, int]]:
        if planet not in self._planets:
            return
        for x_coordinate in range(len(self._map)):
            for y_coordinate in range(len(self._map[x_coordinate])):
                if planet == self._map[x_coordinate][y_coordinate]:
                    return x_coordinate, y_coordinate

    def get_planet_at_coordinates(self, x: int, y: int) -> Optional[Planet]:
        if not self._map[x][y]:
            return
        return self._map[x][y]

    def get_distance_between_planets(self, from_planet: Planet, to_planet: Planet) -> float:
        from_x, from_y = self.get_coordinates_for_planet(from_planet)
        to_x, to_y = self.get_coordinates_for_planet(to_planet)
        return round(hypot(to_x - from_x, to_y - from_y), 2)

    # Debug method
    def debug_print_map(self) -> None:
        for x_coordinate in range(len(self._map)):
            for y_coordinate in range(len(self._map[x_coordinate])):
                if not self._map[x_coordinate][y_coordinate]:
                    print(f"[{x_coordinate}-{y_coordinate}]", end=" ")
                else:
                    print(f"[{self._map[x_coordinate][y_coordinate].name}]", end=" ")
