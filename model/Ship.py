from typing import Optional

from model.Inventory import Inventory


class Ship:
    """
    The Ship class is the base class that defines interfaces and properties that all subsequent ships will have
    """

    def __init__(self, ship_name: str,
                 hit_points: int,
                 base_price: int,
                 inventory: Optional[Inventory] = None,
                 engine_efficiency: float = 13.5):
        # Base info
        self._ship_name: str = ship_name
        self._max_hit_points: int = hit_points
        self._current_hit_points: int = hit_points
        self._base_price: int = base_price
        self._engine_efficiency: float = engine_efficiency

        # Cargo
        self._inventory = inventory if inventory else Inventory()

        # Crew
        self._max_crew_capacity: int = 0
        self._current_crew_capacity: int = 0

        # Armor
        self._max_shield_orbs: int = 0
        self._current_shield_orbs: int = 0
        self._can_equip_kinetic_dampeners: bool = False
        self._has_kinetic_dampeners_equipped: bool = False

        # Weapons
        self._max_laser_capacity: int = 0
        self._current_laser_capacity: int = 0

    @property
    def name(self) -> str:
        return self._ship_name

    @name.setter
    def name(self, new_name: str) -> None:
        if not new_name:
            return
        self._ship_name = new_name

    @property
    def max_hit_points(self) -> int:
        return self._max_hit_points

    @property
    def hit_points(self) -> int:
        return self._current_hit_points

    @hit_points.setter
    def hit_points(self, new_hit_points: int) -> None:
        self._current_hit_points = new_hit_points

    @property
    def inventory(self) -> Inventory:
        return self._inventory

    @property
    def max_crew_capacity(self) -> int:
        return self._max_crew_capacity

    @property
    def current_crew_capacity(self) -> int:
        return self._current_crew_capacity

    @current_crew_capacity.setter
    def current_crew_capacity(self, new_current_crew_capacity: int) -> None:
        if new_current_crew_capacity > self._max_crew_capacity:
            return
        self._current_crew_capacity = new_current_crew_capacity

    @property
    def max_shield_orbs(self) -> int:
        return self._max_shield_orbs

    @property
    def current_shield_orbs(self) -> int:
        return self._current_shield_orbs

    @current_shield_orbs.setter
    def current_shield_orbs(self, shield_orb_amount: int) -> None:
        if shield_orb_amount > self._max_shield_orbs:
            self._current_shield_orbs = self._max_shield_orbs
        self._current_shield_orbs = shield_orb_amount

    @property
    def can_equip_kinetic_dampeners(self) -> bool:
        return self._can_equip_kinetic_dampeners

    @property
    def has_kinetic_dampeners_equipped(self) -> bool:
        return self._has_kinetic_dampeners_equipped

    @property
    def max_laser_capacity(self) -> int:
        return self._max_laser_capacity

    @property
    def current_laser_capacity(self) -> int:
        return self._current_laser_capacity

    @property
    def engine_efficiency(self) -> float:
        # @note: Used to calculate travel cost, the smaller the value, the cheaper the flight
        return self._engine_efficiency

    def equip_kinetic_dampeners(self) -> bool:
        if not self._can_equip_kinetic_dampeners or self._has_kinetic_dampeners_equipped:
            return False
        self._has_kinetic_dampeners_equipped = True
        return True

    def take_damage(self, damage_taken: int) -> int:
        # Applies damage and returns actual damage taken
        # ToDo: Apply Armor calculation
        self._current_hit_points -= damage_taken
        if self._current_hit_points < 0:
            self._current_hit_points = 0
        return damage_taken

    def repair(self, repair_amount: int) -> int:
        # Repairs ship and returns new hit points
        self._current_hit_points += repair_amount
        if self._current_hit_points > self._max_hit_points:
            self._current_hit_points = self._max_hit_points
        return self._current_hit_points

    def is_ship_destroyed(self) -> bool:
        return self._current_hit_points <= 0
