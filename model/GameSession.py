from typing import Optional

from model.Player import Player
from model.Ship import Ship
from datetime import datetime

from model.ships.DebugShip import DebugShip
from model.solar_systems.SolarSystems import castanar


class GameSession:
    """
    This class stores the progress and state of a game session
    """
    def __init__(self, player: Optional[Player]):
        self._player = player
        self._created_at = datetime.now()

    def isGameReady(self) -> bool:
        if not self._player:
            return False
        return True

    @property
    def player(self) -> Player:
        return self._player

    @player.setter
    def player(self, new_player: Player) -> None:
        self._player = new_player

    @property
    def created_at(self) -> datetime:
        return self._created_at


class DebugSession(GameSession):
    """
    The class DebugSession is an instance of GameSession with pre-populated values
    """
    def __init__(self):
        system = castanar
        self._player = Player(player_name="Debug Player Name", ship=DebugShip(), _credits=10000, solar_system=system, planet_location=system.planets[0])
        super().__init__(self._player)
