from model.Planet import Planet
from model.SolarSystem import SolarSystem
from model.Tradepost import build_random_trade_post

castanar: SolarSystem = SolarSystem("Castanar", [
            Planet("Rigel", trade_post=build_random_trade_post(tier=2)),
            Planet("Toliman", trade_post=build_random_trade_post(tier=3)),
            Planet("Bungula", trade_post=build_random_trade_post(tier=4)),
            Planet("Tyrador", trade_post=build_random_trade_post(tier=5)),
        ])
