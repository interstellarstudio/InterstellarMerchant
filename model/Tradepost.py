import logging
from copy import deepcopy
from random import randint
from typing import Union, Type

from model.Materials import *
from tabulate import tabulate


class TradePost:
    NOT_ENOUGH_STOCK = -1
    NOT_FOR_SALE = -2

    def __init__(self, offer_table: list[dict]):
        self._offer_table: list[dict] = offer_table

    @property
    def offer_table(self) -> list[dict]:
        return deepcopy(self._offer_table)

    # Debug Method
    def debug_print_offer_table(self) -> None:
        print(tabulate([[v if not isinstance(v, Material) else v.name for v in offer_set.values()] for offer_set in self._offer_table],
                       headers=["Material", "buy", "sell", "in stock"],
                       tablefmt="heavy_grid")
              )

    def buy(self, material: Type[Material], amount: int) -> Union[Material, int]:
        # Returns the materials bought or error code

        for idx, offer in enumerate(self._offer_table):
            if isinstance(offer["material"], material):
                requested_offer = offer
                index = idx
                break
        else:
            return TradePost.NOT_FOR_SALE

        if requested_offer["in_stock"] < amount:
            return TradePost.NOT_ENOUGH_STOCK

        self._offer_table[index]["in_stock"] -= amount
        return material(quantity=amount)

    def sell(self, material: Material) -> int:
        # Returns the credits that the material sold for, or error code
        for idx, offer in enumerate(self._offer_table):
            if isinstance(offer["material"], type(material)):
                requested_offer = offer
                index = idx
                break
        else:
            return TradePost.NOT_FOR_SALE

        self._offer_table[index]["in_stock"] += material.quantity

        return requested_offer["sell_price"] * material.quantity

    def get_total_price(self, material: Type[Material], amount: int) -> int:
        for offer in self._offer_table:
            if isinstance(offer["material"], material):
                requested_offer = offer
                break
        else:
            return TradePost.NOT_FOR_SALE

        return requested_offer["buy_price"] * amount

    def get_selling_price(self, material: Type[Material]) -> int:
        for offer in self._offer_table:
            if isinstance(offer["material"], material):
                requested_offer = offer
                break
        else:
            return TradePost.NOT_FOR_SALE

        return requested_offer["sell_price"]


def build_random_trade_post(tier: int = 1) -> TradePost:
    # @note: Please refer to the pricing distribution document before touching this functions values
    if tier < 1 or tier > 6:
        logging.error(f"Tier {tier} was selected, even though max supported tier is 6. Defaulting to tier 1")
        tier = 1

    # T1
    mn_buy, ls_buy, dc_buy = randint(1, 20), randint(13, 34), randint(7, 42)
    mn_sell, ls_sell, dc_sell = randint(1, mn_buy), randint(13, ls_buy), randint(7, dc_buy)
    mn_stock, ls_stock, dc_stock = randint(50, 500), randint(50, 500), randint(50, 500)
    offer_table = [
        {"material": MolecularNiobium(), "buy_price": mn_buy, "sell_price": mn_sell, "in_stock": mn_stock},
        {"material": LuminousSteel(), "buy_price": ls_buy, "sell_price": ls_sell, "in_stock": ls_stock},
        {"material": DarkChromium(), "buy_price": dc_buy, "sell_price": dc_sell, "in_stock": dc_stock}
    ]

    # T2
    if tier > 1:
        g_buy, pt_buy = randint(71, 212), randint(80, 256)
        g_sell, pt_sell = randint(71, g_buy), randint(80, pt_buy)
        g_stock, pt_stock = randint(50, 500), randint(50, 500)
        offer_table.append({"material": Graphene(), "buy_price": g_buy, "sell_price": g_sell, "in_stock": g_stock})
        offer_table.append({"material": PhotonicTube(), "buy_price": pt_buy, "sell_price": pt_sell, "in_stock": pt_stock})

    # T3
    if tier > 2:
        e_buy, d_buy = randint(52, 301), randint(64, 199)
        e_sell, d_sell = randint(52, e_buy), randint(64, d_buy)
        e_stock, d_stock = randint(50, 500), randint(50, 500)
        offer_table.append({"material": Erythosium(), "buy_price": e_buy, "sell_price": e_sell, "in_stock": e_stock})
        offer_table.append({"material": Axonanium(), "buy_price": d_buy, "sell_price": d_sell, "in_stock": d_stock})

    # T4
    if tier > 3:
        de_buy, ne_buy = randint(102, 407), randint(165, 399)
        de_sell, ne_sell = randint(102, de_buy), randint(165, ne_buy)
        de_stock, ne_stock = randint(50, 500), randint(50, 500)
        offer_table.append({"material": Dextrorus(), "buy_price": de_buy, "sell_price": de_sell, "in_stock": de_stock})
        offer_table.append({"material": Neptunesium(), "buy_price": ne_buy, "sell_price": ne_sell, "in_stock": ne_stock})

    # T5
    if tier > 4:
        v_buy = randint(210, 480)
        v_sell = randint(210, v_buy)
        v_stock = randint(50, 500)
        offer_table.append({"material": Vibronium(), "buy_price": v_buy, "sell_price": v_sell, "in_stock": v_stock})

    # T6
    if tier > 5:
        se_buy, ph_buy = randint(400, 800), randint(487, 1090)
        se_sell, ph_sell = randint(400, se_buy), randint(487, ph_buy)
        se_stock, ph_stock = randint(50, 500), randint(50, 500)
        offer_table.append({"material": Seismoril(), "buy_price": se_buy, "sell_price": se_sell, "in_stock": se_stock})
        offer_table.append({"material": Phosium(), "buy_price": ph_buy, "sell_price": ph_sell, "in_stock": ph_stock})

    return TradePost(offer_table)


# ToDo: Write unittest
if __name__ == "__main__":
    random_trade_post = build_random_trade_post(tier=1)
    random_trade_post.debug_print_offer_table()
    print(random_trade_post.buy(Seismoril, 1))  # Should return 2
    print(random_trade_post.buy(LuminousSteel, 99999))  # Should return 1
    print(random_trade_post.buy(MolecularNiobium, 50).quantity)  # Should return 50
    random_trade_post.debug_print_offer_table()  # Should display 50 MolecularNiobium less than previous print
    print(random_trade_post.sell(MolecularNiobium(100)))  # Should display the current Molecular Niobium sell price * 100
    random_trade_post.debug_print_offer_table()  # Should display 100 MolecularNiobium more than previous print
    print(random_trade_post.get_total_price(LuminousSteel, 1000))  # Should display Luminous Steel buy price * 1000
