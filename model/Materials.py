class Material:
    def __init__(self, name: str = "", quantity: int = 1):
        self._name: str = name
        self._quantity: int = quantity

    @property
    def name(self) -> str:
        return self._name

    @property
    def quantity(self) -> int:
        return self._quantity

    @quantity.setter
    def quantity(self, new_quantity: int) -> None:
        self._quantity = new_quantity


class MolecularNiobium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Molecular Niobium", quantity)


class LuminousSteel(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Luminous Steel", quantity)


class DarkChromium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Dark Chromium", quantity)


class Graphene(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Graphene", quantity)


class PhotonicTube(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Photonic Tube", quantity)


class Erythosium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Erythosium", quantity)


class Axonanium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Axonanium", quantity)


class Dextrorus(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Dextrorus", quantity)


class Neptunesium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Neptunesium", quantity)


class Vibronium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Vibronium", quantity)


class Seismoril(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Seismoril", quantity)


class Phosium(Material):
    def __init__(self, quantity: int = 1):
        super().__init__("Phosium", quantity)
