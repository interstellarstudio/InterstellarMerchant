from typing import Optional, Callable

from PySide6.QtCore import QObject, Signal

from model.GameSession import GameSession
from model.Screens import SCREENS


class GameCoordinatorService(QObject):
    ui_update_required_signal = Signal(int)
    """
    The GameCoordinatorService is the top level service that coordinates the games flow
    """
    def __init__(self, ui_update_required_slot: Callable, session: Optional[GameSession] = None):
        super().__init__()
        self._session: Optional[GameSession] = session
        self.ui_update_required_signal.connect(ui_update_required_slot)
        self._next_view: Optional[int] = None
        self._current_view: Optional[int] = None

    @property
    def session(self) -> Optional[GameSession]:
        return self._session

    @session.setter
    def session(self, new_session: Optional[GameSession]) -> None:
        self._session = new_session

    @property
    def next_view(self) -> Optional[int]:
        if self._next_view:
            next_view = self._next_view
            self._next_view = None
            return next_view

    @property
    def current_view(self) -> int:
        return self._current_view

    @current_view.setter
    def current_view(self, new_view: int) -> None:
        self._current_view = new_view

    def handle_click(self, source: str, button: str) -> None:
        print(f"Got click out of {source}: {button}")
        match source:
            case "MainMenu":
                self._handle_main_menu_click(button)
            case "TopMenuBar":
                self._handle_top_menu_bar_click(button)
            case _:
                pass
        self.ui_update_required_signal.emit(1)

    def _handle_main_menu_click(self, button: str) -> None:
        match button:
            case "startgame":
                self._next_view = SCREENS.STATUS
            case "exit":
                exit(0)
            case _:
                pass

    def _handle_top_menu_bar_click(self, button: str) -> None:
        match button:
            case "status":
                self._next_view = SCREENS.STATUS
            case "tradepost":
                self._next_view = SCREENS.TRADEPOST
            case "travel":
                self._next_view = SCREENS.TRAVEL
            case "spacedock":
                self._next_view = SCREENS.SPACEDOCK
            case "gunrunner":
                self._next_view = SCREENS.GUNRUNNER
            case "rnd":
                self._next_view = SCREENS.RND
            case "options":
                self._next_view = SCREENS.OPTIONS
            case "saveexit":
                self._next_view = SCREENS.MAIN_MENU

