import logging
import sys
from typing import Optional

from PySide6 import QtWidgets
from PySide6.QtCore import Slot
from PySide6.QtGui import QFontDatabase
from PySide6.QtWidgets import QMainWindow, QVBoxLayout, QWidget
from from_root import from_root

from model.GameSession import GameSession, DebugSession
from model.Screens import SCREENS
from service.GameCoordinatorService import GameCoordinatorService
from ui_components.DebugView import DebugView
from ui_components.MainMenu import MainMenu
from ui_components.StatusView import StatusView
from ui_components.TopMenuBar import TopMenuBar
from ui_components.TravelView import TravelView
from ui_components.TradePostView import TradePostView


class InterstellarMerchantMainWindow(QMainWindow):
    def __init__(self, game_session: Optional[GameSession] = None):
        super().__init__()
        # Game setup
        self._game_session: Optional[GameSession] = game_session
        self._game_coordinator_service = GameCoordinatorService(self.ui_update_required_slot, session=self._game_session)

        # GUI setup
        self.setWindowTitle("Interstellar Merchant")
        self.setFixedSize(1280, 720)  # 720p
        self._layout = QVBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(0)
        QFontDatabase.addApplicationFont(str(from_root("style", "fonts", "RobotoMonoRegular.ttf")))

        # Central Widget
        self._central_widget = QWidget()
        self._central_widget.setLayout(self._layout)
        self.setCentralWidget(self._central_widget)

        # UI Components
        self._top_menu_bar = TopMenuBar(self._game_coordinator_service)
        self._main_menu = MainMenu(self._game_coordinator_service)
        self._main_view_widget = QWidget()
        self._main_view_widget.setFixedSize(1280, 660)
        main_view_layout = QVBoxLayout()
        main_view_layout.setContentsMargins(0, 0, 0, 0)
        main_view_layout.setSpacing(0)
        self._main_view_widget.setLayout(main_view_layout)
        self._layout.addWidget(self._top_menu_bar)
        self._layout.addWidget(self._main_view_widget)
        self._layout.addWidget(self._main_menu)

        # ToDo: Implement session loading and always start in main menu
        if not self._game_session:
            self._current_screen = SCREENS.MAIN_MENU
        else:
            self._current_screen = SCREENS.TRADEPOST

        self._build_screen()

    def _build_screen(self) -> None:
        if self._current_screen == SCREENS.EMPTY:
            self._main_menu.hide()
            self._top_menu_bar.hide()
            self._main_view_widget.hide()
        elif self._current_screen == SCREENS.DEBUG:
            self._switch_main_view(True)
        elif self._current_screen == SCREENS.MAIN_MENU:
            self._switch_main_view(False)
        elif self._current_screen == SCREENS.OPTIONS:
            self._switch_main_view(True)
            self._set_main_view_to_widget(DebugView("SCREENS.OPTIONS"))
        elif self._current_screen == SCREENS.STATUS:
            self._switch_main_view(True)
            self._set_main_view_to_widget(StatusView(self._game_coordinator_service))
        elif self._current_screen == SCREENS.TRADEPOST:
            self._switch_main_view(True)
            self._set_main_view_to_widget(TradePostView(self._game_coordinator_service))
        elif self._current_screen == SCREENS.TRAVEL:
            self._switch_main_view(True)
            self._set_main_view_to_widget(TravelView(self._game_coordinator_service))
        elif self._current_screen == SCREENS.SPACEDOCK:
            self._switch_main_view(True)
            self._set_main_view_to_widget(DebugView("SCREENS.SPACEDOCK"))
        elif self._current_screen == SCREENS.GUNRUNNER:
            self._switch_main_view(True)
            self._set_main_view_to_widget(DebugView("SCREENS.GUNRUNNER"))
        elif self._current_screen == SCREENS.RND:
            self._switch_main_view(True)
            self._set_main_view_to_widget(DebugView("SCREENS.RND"))
        self._game_coordinator_service.current_view = self._current_screen

    def _switch_main_view(self, active: bool) -> None:
        if active:
            self._main_menu.hide()
            self._top_menu_bar.show()
            self._main_view_widget.show()
        else:
            self._main_menu.show()
            self._top_menu_bar.hide()
            self._main_view_widget.hide()

    def _set_main_view_to_widget(self, widget: QWidget) -> None:
        while not self._main_view_widget.layout().isEmpty():
            self._main_view_widget.layout().takeAt(0)
        self._main_view_widget.layout().addWidget(widget)

    @Slot(int)
    def ui_update_required_slot(self, _):
        # If this slot receives a signal, it is time to poll information and update the UI
        print("updating the UI...")
        next_view = self._game_coordinator_service.next_view
        if next_view:
            self._current_screen = next_view
        self._build_screen()


if __name__ == "__main__":
    debug_mode_on = True
    application = QtWidgets.QApplication([])
    if debug_mode_on:
        main_window = InterstellarMerchantMainWindow(DebugSession())
        logging.basicConfig(encoding="utf-8", level=logging.DEBUG)
    else:
        main_window = InterstellarMerchantMainWindow()
        logging.basicConfig(encoding="utf-8", level=logging.ERROR)
    main_window.show()

    sys.exit(application.exec())
