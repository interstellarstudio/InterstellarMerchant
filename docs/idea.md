# Interstellar Merchant Idea

You are a lowly merchant from a poor planet. You just inherited your uncles old merchant ship.

You can now reach neighboring planets but cant leave the system. You have to make money to afford a better ship, which will open up additional markets

While traveling between planets you can encounter pirates, lawmen, other merchants, civilians, aliens  and other things.

You will need to get a crew, upgrade your ship, buy equipments to survive your travels.

## Pinpoints

- Focus on traveling and trading
- Combat should be simple but engaging
- Questlines?

## Important first things

- Basic UI with buttons and displays
- Rigid datamodels

## Gameplay loop (long)

- Trade to make money
- Take money to upgrade ship
- Upgraded ship can travel further, making more money
- More money buys bigger ship
- Bigger ship can travel solar systems
- New solar systems offer more money and quests
- Reach last solar system

## Gameplay loop (short)

- Buy wares at destination A
- Travel to destination B
- Sell wares at Destination B
- Spent profits