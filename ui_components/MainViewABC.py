from PySide6.QtWidgets import QWidget


class MainViewABC(QWidget):
    """
    The MainViewABC class is the base class for all view widgets that are displayed on the main view
    """
    def __init__(self):
        super().__init__()
        self.setFixedSize(1280, 660)
