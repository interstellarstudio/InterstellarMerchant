from copy import deepcopy
from datetime import datetime
from typing import Optional

from PySide6.QtCore import QTimer
from PySide6.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, QWidget, QLabel, QGridLayout, QListWidget, \
    QListWidgetItem
from from_root import from_root

from model.Ship import Ship
from service.GameCoordinatorService import GameCoordinatorService
from ui_components.MainViewABC import MainViewABC


class InfoBox(QWidget):
    FULL_HEIGHT = 660
    HALF_HEIGHT = 330

    def __init__(self, height: int, parent: Optional[QWidget]):
        super().__init__(parent=parent)
        self.setFixedSize(427, height)
        self._background_label = QLabel("", self)
        self._background_label.setFixedSize(427, height)
        self._background_label.setWordWrap(True)
        self._background_label.setObjectName("BorderLabel")


class PlayerStatusBox(InfoBox):
    class PlayerStatusBoxText(QLabel):
        def __init__(self, text: str, style_sheet: str):
            super().__init__(text)
            self.setObjectName("PlayerStatusBoxText")
            self.setStyleSheet(style_sheet)
            self.setWordWrap(True)

    def __init__(self, height: int, parent: Optional[QWidget]):
        super().__init__(height, parent)

        with open(from_root("style", "PlayerStatusBox.qss"), "r") as style_file:
            style_sheet = style_file.read()
            self._background_label.setStyleSheet(style_sheet)

        self._name_label = PlayerStatusBox.PlayerStatusBoxText("", style_sheet)
        self._credits_label = PlayerStatusBox.PlayerStatusBoxText("", style_sheet)
        self._time_label = PlayerStatusBox.PlayerStatusBoxText("", style_sheet)
        self._location_label = PlayerStatusBox.PlayerStatusBoxText("", style_sheet)

        self._grid = QGridLayout()
        self.setLayout(self._grid)
        self.setContentsMargins(65, 30, 5, 10)
        self._grid.addWidget(PlayerStatusBox.PlayerStatusBoxText("Name:", style_sheet), 1, 1)
        self._grid.addWidget(self._name_label, 1, 2)
        self._grid.addWidget(PlayerStatusBox.PlayerStatusBoxText("Current location:", style_sheet), 2, 1)
        self._grid.addWidget(self._location_label, 2, 2)
        self._grid.addWidget(PlayerStatusBox.PlayerStatusBoxText("Time elapsed:", style_sheet), 3, 1)
        self._grid.addWidget(self._time_label, 3, 2)
        self._grid.addWidget(PlayerStatusBox.PlayerStatusBoxText("Credits:", style_sheet), 4, 1)
        self._grid.addWidget(self._credits_label, 4, 2)

    def set_name(self, name: str) -> None:
        self._name_label.setText(f"<b>{name}</b>")

    def set_credits(self, _credits: str) -> None:
        self._credits_label.setText(f"<b>{_credits}</b>")

    def set_time(self, time: str) -> None:
        self._time_label.setText(f"<b>{time}</b>")

    def set_location(self, location: str) -> None:
        self._location_label.setText(f"<b>{location}</b>")


class ShipStatusBox(InfoBox):
    class ShipStatusBoxText(QLabel):
        def __init__(self, text: str, style_sheet: str):
            super().__init__(text)
            self.setObjectName("ShipStatusBoxText")
            self.setStyleSheet(style_sheet)
            self.setWordWrap(True)

    def __init__(self, height: int, parent: Optional[QWidget]):
        super().__init__(height, parent)

        with open(from_root("style", "ShipStatusBox.qss"), "r") as style_file:
            style_sheet = style_file.read()
            self._background_label.setStyleSheet(style_sheet)

        self._name_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._hit_points_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._cargo_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._crew_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._shields_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._kinetic_dampeners_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)
        self._laser_label = ShipStatusBox.ShipStatusBoxText("", style_sheet)

        self._grid = QGridLayout()
        self.setLayout(self._grid)
        self.setContentsMargins(65, 40, 5, 10)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Name:", style_sheet), 1, 1)
        self._grid.addWidget(self._name_label, 1, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Hit Points:", style_sheet), 2, 1)
        self._grid.addWidget(self._hit_points_label, 2, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Crew:", style_sheet), 3, 1)
        self._grid.addWidget(self._crew_label, 3, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Cargo Capacity:", style_sheet), 4, 1)
        self._grid.addWidget(self._cargo_label, 4, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Shields:", style_sheet), 5, 1)
        self._grid.addWidget(self._shields_label, 5, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Lasers:", style_sheet), 6, 1)
        self._grid.addWidget(self._laser_label, 6, 2)
        self._grid.addWidget(ShipStatusBox.ShipStatusBoxText("Kinetic Dampeners:", style_sheet), 7, 1)
        self._grid.addWidget(self._kinetic_dampeners_label, 7, 2)

    def set_data(self, ship: Ship) -> None:
        self._name_label.setText(f"<b>{ship.name}</b>")
        self._hit_points_label.setText(f"<b>{ship.hit_points} / {ship.max_hit_points}</b>")
        self._cargo_label.setText(f"<b>{ship.inventory.current_cargo_capacity} / {ship.inventory.max_cargo_capacity}</b>")
        self._crew_label.setText(f"<b>{ship.current_crew_capacity} / {ship.max_crew_capacity}</b>")
        self._shields_label.setText(f"<b>{ship.current_shield_orbs} / {ship.max_shield_orbs}</b>")
        self._laser_label.setText(f"<b>{ship.current_laser_capacity} / {ship.max_laser_capacity}</b>")
        self._kinetic_dampeners_label.setText(f"<b>{'equipped' if ship.has_kinetic_dampeners_equipped else '- none -'}</b>")


class CargoStatusBox(InfoBox):
    SORT_ALPHA = 1
    SORT_ALPHA_REVERSED = 2
    SORT_QUANTITY = 3
    SORT_QUANTITY_REVERSED = 4

    class CargoStatusBoxText(QLabel):
        def __init__(self, text: str, style_sheet: str):
            super().__init__(text)
            self.setObjectName("CargoStatusBoxText")
            self.setStyleSheet(style_sheet)
            self.setWordWrap(True)

    def __init__(self, height: int, game_coordinator_service: GameCoordinatorService, parent: Optional[QWidget]):
        super().__init__(height, parent)

        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service
        self._list_widget = QListWidget(self)
        self._list_widget.setObjectName("CargoStatusBoxList")
        self._list_widget.setFixedSize(392, 560)

        # Sorting UI
        self._current_sort = None
        self._sort_label = QLabel("Sort Cargo:", self)
        self._sort_label.setObjectName("SortLabel")
        self._sort_quantity_button = QPushButton("123 ", self)
        self._sort_quantity_button.setObjectName("SortQuantityButton")
        self._sort_quantity_button.setFlat(True)
        self._sort_quantity_button.clicked.connect(lambda: self._handle_sort_button_click("SortQuantityButton"))
        self._sort_alphabetical_button = QPushButton("ABC ", self)
        self._sort_alphabetical_button.setObjectName("SortAlphaButton")
        self._sort_alphabetical_button.setFlat(True)
        self._sort_alphabetical_button.clicked.connect(lambda: self._handle_sort_button_click("SortAlphaButton"))

        self.set_list_items()

        with open(from_root("style", "CargoStatusBox.qss"), "r") as style_file:
            style_sheet = style_file.read()
            self._background_label.setStyleSheet(style_sheet)
            self._list_widget.setStyleSheet(style_sheet)
            self._sort_alphabetical_button.setStyleSheet(style_sheet)
            self._sort_quantity_button.setStyleSheet(style_sheet)
            self._sort_label.setStyleSheet(style_sheet)

    def set_list_items(self, sort_by=SORT_ALPHA) -> None:
        self._current_sort = sort_by
        self._list_widget.clear()
        inventory_list = self._game_coordinator_service.session.player.ship.inventory.get_inventory_list()
        # ToDo: Make prettier
        for material in sorted(inventory_list, key=lambda elem: elem.name if sort_by < 3 else elem.quantity, reverse=True if sort_by == CargoStatusBox.SORT_ALPHA_REVERSED or sort_by == CargoStatusBox.SORT_QUANTITY_REVERSED else False):
            item = QListWidgetItem(f"[{str(material.quantity) + ']':<4} {material.name}")
            item.setFont("Roboto Mono")
            self._list_widget.addItem(item)

    def _handle_sort_button_click(self, source: str) -> None:
        if source == "SortAlphaButton":
            if self._current_sort == CargoStatusBox.SORT_ALPHA:
                self.set_list_items(CargoStatusBox.SORT_ALPHA_REVERSED)
            elif self._current_sort == CargoStatusBox.SORT_ALPHA_REVERSED:
                self.set_list_items(CargoStatusBox.SORT_ALPHA)
            else:
                self.set_list_items(CargoStatusBox.SORT_ALPHA)
        elif source == "SortQuantityButton":
            if self._current_sort == CargoStatusBox.SORT_QUANTITY:
                self.set_list_items(CargoStatusBox.SORT_QUANTITY_REVERSED)
            elif self._current_sort == CargoStatusBox.SORT_QUANTITY_REVERSED:
                self.set_list_items(CargoStatusBox.SORT_QUANTITY)
            else:
                self.set_list_items(CargoStatusBox.SORT_QUANTITY)


class CrewStatusBox(InfoBox):
    class CrewStatusBoxText(QLabel):
        def __init__(self, text: str, style_sheet: str):
            super().__init__(text)
            self.setObjectName("CrewStatusBoxText")
            self.setStyleSheet(style_sheet)
            self.setWordWrap(True)

    def __init__(self, height: int, parent: Optional[QWidget]):
        super().__init__(height, parent)

        with open(from_root("style", "CrewStatusBox.qss"), "r") as style_file:
            style_sheet = style_file.read()
            self._background_label.setStyleSheet(style_sheet)

        self._feature_not_implemented_label = CrewStatusBox.CrewStatusBoxText("This feature is\nnot implemented yet", style_sheet)
        self._grid = QGridLayout()
        self._grid.addWidget(self._feature_not_implemented_label, 3, 3)
        self.setLayout(self._grid)
        self.setContentsMargins(150, 0, 0, 400)  # TBD


class StatusView(MainViewABC):
    def __init__(self, game_coordinator_service: GameCoordinatorService):
        super().__init__()
        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service
        self._v_layout = QVBoxLayout()
        self._v_layout.setContentsMargins(0, 0, 0, 0)
        self._v_layout.setSpacing(0)
        self._h_layout = QHBoxLayout()
        self._h_layout.setContentsMargins(0, 0, 0, 0)
        self._h_layout.setSpacing(0)
        self._h_layout.addLayout(self._v_layout)
        self.setLayout(self._h_layout)

        self._info_box_top_left = PlayerStatusBox(InfoBox.HALF_HEIGHT, self)
        self._info_box_bottom_left = ShipStatusBox(InfoBox.HALF_HEIGHT, self)
        self._info_box_middle = CargoStatusBox(InfoBox.FULL_HEIGHT, self._game_coordinator_service, self)
        self._info_box_right = CrewStatusBox(InfoBox.FULL_HEIGHT, self)

        self._v_layout.addWidget(self._info_box_top_left)
        self._v_layout.addWidget(self._info_box_bottom_left)
        self._h_layout.addWidget(self._info_box_middle)
        self._h_layout.addWidget(self._info_box_right)

        # Populate
        if self._game_coordinator_service.session:
            self._info_box_top_left.set_name(self._game_coordinator_service.session.player.name)
            self._info_box_top_left.set_credits(str(self._game_coordinator_service.session.player.credits))
            self._info_box_top_left.set_location(self._game_coordinator_service.session.player.get_full_location())
            self.update_time()
            self._info_box_bottom_left.set_data(deepcopy(self._game_coordinator_service.session.player.ship))
        else:
            self._info_box_top_left.set_name("NO SESSION")
            self._info_box_top_left.set_time("NO SESSION")
            self._info_box_top_left.set_credits("NO SESSION")
            self._info_box_top_left.set_location("NO SESSION")

        self._elapsed_time_update_timer = QTimer()
        self._elapsed_time_update_timer.timeout.connect(self.update_time)
        self._elapsed_time_update_timer.start(1000)

    def update_time(self) -> None:
        hours, remainder = divmod((datetime.now() - self._game_coordinator_service.session.created_at).seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        self._info_box_top_left.set_time("{:02} : {:02} : {:02}".format(int(hours), int(minutes), int(seconds)))
