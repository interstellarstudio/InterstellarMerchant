import logging
from typing import Optional, Callable

from PySide6.QtCore import Qt, QSize, Signal, Slot
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QPushButton, QWidget, QLabel, QGridLayout, QToolButton, \
    QListWidget, QListWidgetItem
from from_root import from_root

from model.Planet import Planet
from model.Player import Player
from model.SolarSystem import SolarSystem
from service.GameCoordinatorService import GameCoordinatorService
from ui_components.MainViewABC import MainViewABC


class DepartureModal(QWidget):
    update_boxes_signal = Signal(int)

    def __init__(self, parent: Optional[QWidget],
                 game_coordinator_service: GameCoordinatorService,
                 update_boxes_slot: Callable):
        super().__init__(parent=parent)
        # Base setup
        self.setGeometry(0, 0, 1280, 660)
        self.setObjectName("DepartureModal")
        self.update_boxes_signal.connect(update_boxes_slot)
        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service
        self._destination: Optional[Planet] = None
        self._cost: Optional[int] = None

        # Background
        self._background_label = QLabel("", self)
        self._background_label.setObjectName("DepartureModalBackground")

        # Text & Controls
        self._confirmation_text = QLabel("", self)
        self._confirmation_text.setWordWrap(True)
        self._confirmation_text.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self._confirmation_text.setObjectName("DepartureModalText")

        self._abort_button = QPushButton("", self)
        self._abort_button.setObjectName("DepartureModalAbortButton")
        self._abort_button.clicked.connect(lambda: self.hide())

        self._confirm_button = QPushButton("", self)
        self._confirm_button.setObjectName("DepartureModalConfirmButton")
        self._confirm_button.clicked.connect(self._commence_travel)

    def prepare_modal(self, from_planet: Planet, to_planet: Planet, player: Player) -> None:
        travel_distance = player.system_location.get_distance_between_planets(from_planet, to_planet)
        cost = round(player.ship.engine_efficiency * travel_distance)
        self._confirmation_text.setText(f"Traveling from {from_planet.name} to {to_planet.name} will cost <b>{cost}</b>"
                                        f" credits. You currently have <b>{player.credits}</b> credits.")
        if cost > player.credits:
            self._confirm_button.setDisabled(True)
        else:
            self._confirm_button.setDisabled(False)

        self._destination = to_planet
        self._cost = cost

    def _commence_travel(self) -> None:
        self._game_coordinator_service.session.player.remove_credits(self._cost)
        if not self._game_coordinator_service.session.player.travel_to_planet(self._destination):
            logging.error(f"Error while traveling to {self._destination.name}")
        else:
            self._destination = None
            self._cost = None
            self.update_boxes_signal.emit(1)
            self.hide()


class PlanetInfo(QWidget):
    def __init__(self, parent: Optional[QWidget]):
        super().__init__(parent=parent)
        # Base setup
        self.setFixedSize(427, 500)
        self.setGeometry(0, 0, 427, 500)
        self.setObjectName("PlanetInfo")

        # Background
        self._background_label = QLabel("", self)
        self._background_label.setFixedSize(427, 500)
        self._background_label.setWordWrap(True)
        self._background_label.setObjectName("PlanetInfoBackground")

        # Planet name
        self._planet_name_label = QLabel("", self)
        self._planet_name_label.setFixedWidth(295)
        self._planet_name_label.setObjectName("PlanetName")
        self._planet_name_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        # Trading Post Table
        self._list_widget = QListWidget(self)
        self._list_widget.setObjectName("TradePostOfferTableList")
        self._list_widget.setFixedSize(410, 472)
        self._list_widget.setDisabled(True)

    def set_planet_info(self, new_planet: Planet) -> None:
        self._list_widget.clear()
        table_head = QListWidgetItem("Material           Buy    Sell    Stock\n\n")
        table_head.setFont("Roboto Mono")
        self._list_widget.addItem(table_head)
        for offer in new_planet.trade_post.offer_table:
            item = QListWidgetItem(f"{offer['material'].name:<19}{offer['buy_price']:<7}{offer['sell_price']:<8}{offer['in_stock']}")
            item.setFont("Roboto Mono")
            self._list_widget.addItem(item)
        self._planet_name_label.setText(new_planet.name)


class TravelControl(QWidget):
    def __init__(self, parent: Optional[QWidget]):
        super().__init__(parent=parent)
        # Base setup
        self.setFixedSize(427, 160)
        self.setGeometry(0, 500, 427, 160)
        self.setObjectName("TravelControl")
        self._current_planet_selection = None  # Set this to None if current planet is the planet the player is on.

        # Background
        self._background_label = QLabel("", self)
        self._background_label.setFixedSize(427, 160)
        self._background_label.setWordWrap(True)
        self._background_label.setObjectName("TravelControlBackground")

        # Text & Controls
        self._wormhole_generator_button = QPushButton("use Wormhole", self)
        self._wormhole_generator_button.setObjectName("TravelControlButtonWormhole")
        self._wormhole_generator_button.setDisabled(True)  # ToDo: Activate together with wormhole feature

        self._travel_button = QPushButton("Depart", self)
        self._travel_button.setObjectName("TravelControlButtonDepart")
        self._travel_button.setDisabled(True)
        self._travel_button.clicked.connect(lambda: self.parent().open_travel_dialogue(self._current_planet_selection))

        self._travel_label = QLabel("Can not travel\nAlready there...", self)
        self._travel_label.setFixedSize(410, 70)
        self._travel_label.setWordWrap(True)
        self._travel_label.setObjectName("TravelControlLabel")

    @property
    def current_planet_selection(self) -> Optional[Planet]:
        return self._current_planet_selection

    @current_planet_selection.setter
    def current_planet_selection(self, new_selection: Optional[Planet]) -> None:
        self._current_planet_selection = new_selection
        if not self._current_planet_selection:
            self._travel_label.setText("Can not travel\nAlready there...")
            self._travel_button.setDisabled(True)
        else:
            self._travel_label.setText(f"Travel to {new_selection.name} ?")
            self._travel_button.setDisabled(False)


class SystemMap(QWidget):
    class PlanetButton(QToolButton):
        def __init__(self, planet: Optional[Planet] = None):
            super().__init__()
            self.setFixedSize(70, 70)
            self.setAutoRaise(True)
            if not planet:
                self.setDisabled(True)
            if planet:
                random_icon = QIcon(str(from_root("style", "img", "planets", f"planet_{planet.style}.png")))
                self.setIcon(random_icon)
                self.setIconSize(QSize(45, 45))
                self.setText(planet.name)
                self.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonTextUnderIcon)
                self.clicked.connect(lambda: self.parent().parent().parent().select_planet(planet))

    def __init__(self, parent: Optional[QWidget]):
        super().__init__(parent=parent)

        # Setup
        self.setFixedSize(854, 660)
        self.setGeometry(427, 0, 854, 660)
        self.setObjectName("SystemMap")

        # Background
        self._background_label = QLabel("", self)
        self._background_label.setFixedSize(854, 660)
        self._background_label.setWordWrap(True)
        self._background_label.setObjectName("SystemMapBackground")

        # System Name
        self._system_name_label = QLabel("", self)
        self._system_name_label.setFixedWidth(340)
        self._system_name_label.setObjectName("SystemName")
        self._system_name_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        # Layout/Box for planets
        self._map_box = self._construct_new_map_box()

    def set_system_info(self, new_system: SolarSystem) -> None:
        self._map_box = self._construct_new_map_box()
        self._system_name_label.setText(new_system.name)
        for x in range(0, 6):
            for y in range(0, 6):
                self._map_box.layout().addWidget(SystemMap.PlanetButton(new_system.get_planet_at_coordinates(x, y)), x, y)

    def _construct_new_map_box(self) -> QWidget:
        map_box = QWidget(self)
        map_box.setGeometry(60, 80, 760, 510)
        layout = QGridLayout()
        layout.setSpacing(5)
        map_box.setLayout(layout)
        return map_box


class TravelView(MainViewABC):
    # Note: There were some problems with getting the layouts to work here.
    #       So all coordinates and sizes are hardcoded for the time being. Maybe revisit later.
    def __init__(self, game_coordinator_service: GameCoordinatorService):
        super().__init__()
        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service

        # Load styles
        with open(from_root("style", "TravelView.qss"), "r") as style_file:
            self._style_sheet = style_file.read()

        # Components
        self._planet_info = PlanetInfo(self)
        self._travel_control = TravelControl(self)
        self._system_map = SystemMap(self)
        self._departure_modal = DepartureModal(self, self._game_coordinator_service, self.update_boxes_slot)
        self._departure_modal.hide()

        self.setStyleSheet(self._style_sheet)

        self.update_boxes(self._game_coordinator_service.session.player.system_location, self._game_coordinator_service.session.player.planet_location)

    def update_boxes(self, system: Optional[SolarSystem], planet: Optional[Planet]) -> None:
        if system:
            self._system_map.set_system_info(system)
        if planet:
            self._planet_info.set_planet_info(planet)

    def select_planet(self, planet: Planet) -> None:
        self.update_boxes(None, planet)
        self._travel_control.current_planet_selection = planet if planet != self._game_coordinator_service.session.player.planet_location else None

    def open_travel_dialogue(self, destination: Optional[Planet]) -> None:
        if not destination:  # Shouldn't happen, but just in case
            return
        self._departure_modal.prepare_modal(self._game_coordinator_service.session.player.planet_location,
                                            destination, self._game_coordinator_service.session.player)
        self._departure_modal.show()

    @Slot(int)
    def update_boxes_slot(self, _):
        # If this slot receives a signal, it is time to poll information and update the UI
        self._travel_control.current_planet_selection = None
        self.update_boxes(
            self._game_coordinator_service.session.player.system_location,
            self._game_coordinator_service.session.player.planet_location
        )
