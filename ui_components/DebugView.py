from PySide6 import QtCore
from PySide6.QtGui import QPalette, QColor
from PySide6.QtWidgets import QLabel

from ui_components.MainViewABC import MainViewABC


class DebugView(MainViewABC):
    """
    Simple ViewWidget that displays the name of the route
    """
    def __init__(self, name: str):
        super().__init__()
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QPalette.Window, QColor("#1A1A1A"))
        self.setPalette(palette)

        self._name_label = QLabel(name, self)
        self._name_label.setFixedSize(self.width(), self.height())
        self._name_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self._name_label.setStyleSheet("color: white; font-size: 35px")
