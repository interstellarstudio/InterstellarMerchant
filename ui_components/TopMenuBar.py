from from_root import from_root
from PySide6.QtCore import QRect, QTimer
from PySide6.QtGui import QColor, QPalette
from PySide6.QtWidgets import QWidget, QPushButton, QHBoxLayout, QSpacerItem

from model.Screens import SCREENS
from service.GameCoordinatorService import GameCoordinatorService


class TopMenuBarButton(QPushButton):
    # ToDo: The click effect looks a bit ugly, maybe subclass QLabel and emit a click signal instead.
    def __init__(self, text: str):
        super().__init__(text)
        self.setFlat(True)
        self.setFixedWidth(110)
        self.setFixedHeight(50)


class TopMenuBar(QWidget):
    def __init__(self, game_coordinator_service: GameCoordinatorService):
        super().__init__()
        # Styling
        self.setAutoFillBackground(True)
        self._layout = QHBoxLayout()
        self.setContentsMargins(0, 0, 0, 0)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self._layout)

        palette = self.palette()
        palette.setColor(QPalette.Window, QColor("#1A1A1A"))
        self.setPalette(palette)

        self.setFixedHeight(60)

        with open(from_root("style", "TopMenuBar.qss"), "r") as qss_file:
            self.setStyleSheet(qss_file.read())

        # Functionality
        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service
        self._buttons: dict = {
            "status": TopMenuBarButton("Status"),
            "tradepost": TopMenuBarButton("Tradepost"),
            "travel": TopMenuBarButton("Travel"),
            "spacedock": TopMenuBarButton("Space Dock"),
            "gunrunner": TopMenuBarButton("Gun Runner"),
            "rnd": TopMenuBarButton("Research && \nDevelopment"),
            "options": TopMenuBarButton("Options"),
            "saveexit": TopMenuBarButton("Save && Exit")
        }

        self._buttons["status"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "status"))
        self._buttons["tradepost"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "tradepost"))
        self._buttons["travel"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "travel"))
        self._buttons["spacedock"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "spacedock"))
        self._buttons["gunrunner"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "gunrunner"))
        self._buttons["rnd"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "rnd"))
        self._buttons["options"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "options"))
        self._buttons["options"].setEnabled(False)
        self._buttons["saveexit"].clicked.connect(lambda: self._game_coordinator_service.handle_click("TopMenuBar", "saveexit"))

        for idx, b in enumerate(self._buttons.values()):
            self._layout.addWidget(b)
            b.clicked.connect(self._check_for_ui_update)
            if idx == 5:
                self._layout.addSpacing(280)

        # Note: Do a UI update check after the application has been started to correct for any changes during initialization
        self._start_timer = QTimer()
        self._start_timer.timeout.connect(self._check_for_ui_update)
        self._start_timer.setSingleShot(True)
        self._start_timer.start(50)

    def _check_for_ui_update(self) -> None:
        # Activate Buttons
        for b in self._buttons.values():
            b.setStyleSheet("background-image: url('style/img/button_blank_110x50.png');")
        match self._game_coordinator_service.current_view:
            case SCREENS.STATUS:
                self._buttons["status"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.TRADEPOST:
                self._buttons["tradepost"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.TRAVEL:
                self._buttons["travel"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.SPACEDOCK:
                self._buttons["spacedock"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.GUNRUNNER:
                self._buttons["gunrunner"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.RND:
                self._buttons["rnd"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case SCREENS.OPTIONS:
                self._buttons["options"].setStyleSheet("background-image: url('style/img/button_blank_active_110x50.png');")
            case _:
                pass
