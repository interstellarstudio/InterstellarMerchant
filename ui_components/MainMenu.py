from PySide6 import QtCore
from PySide6.QtGui import QMovie
from PySide6.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLabel
from from_root import from_root

from service.GameCoordinatorService import GameCoordinatorService


class MainMenuButton(QPushButton):
    def __init__(self, text: str):
        super().__init__(text)
        self.setFixedWidth(110)
        self.setFixedHeight(50)


class MainMenu(QWidget):
    def __init__(self, game_coordinator_service: GameCoordinatorService):
        super().__init__()
        self._game_coordinator_service = game_coordinator_service

        # Styling
        self.setFixedSize(1280, 720)

        # Animated Background
        self._background_animation = QMovie(str(from_root("style", "img", "space_animated_01.gif")))
        for x in range(0, self.width(), 200):
            for y in range(0, self.height(), 200):
                background_widget = QLabel("", self)
                background_widget.setGeometry(x, y, 200, 200)
                background_widget.setMovie(self._background_animation)
        self._background_animation.start()

        # Menu
        self._layout = QVBoxLayout()
        self.setContentsMargins(0, 0, 0, 0)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self._layout)
        self.setFixedHeight(720)
        self._title_label = QLabel()
        self._title_label.setStyleSheet("background-image: url('style/img/logo.png');")
        self._title_label.setFixedSize(1000, 181)
        self._layout.addSpacing(50)
        self._layout.addWidget(self._title_label, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

        with open(from_root("style", "MainMenu.qss"), "r") as qss_file:
            self.setStyleSheet(qss_file.read())

        # Functionality
        self._buttons: dict = {
            "startgame": MainMenuButton("Start Game"),
            "loadgame": MainMenuButton("Load Game"),
            "options": MainMenuButton("Options"),
            "about": MainMenuButton("About"),
            "website": MainMenuButton("Website"),
            "exit": MainMenuButton("Exit"),
        }

        self._buttons["startgame"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "startgame"))
        self._buttons["loadgame"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "loadgame"))
        self._buttons["options"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "options"))
        self._buttons["about"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "about"))
        self._buttons["website"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "website"))
        self._buttons["exit"].clicked.connect(lambda: self._game_coordinator_service.handle_click("MainMenu", "exit"))

        self._buttons["loadgame"].setEnabled(False)
        self._buttons["options"].setEnabled(False)
        self._buttons["about"].setEnabled(False)
        self._buttons["website"].setEnabled(False)

        self._layout.addSpacing(100)
        for idx, b in enumerate(self._buttons.values()):
            self._layout.addWidget(b, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
        self._layout.addSpacing(100)
