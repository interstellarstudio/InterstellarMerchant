import logging
from typing import Union, Type

from PySide6.QtCore import QTimer
from PySide6.QtWidgets import QPushButton, QWidget, QLabel, QListWidget, QListWidgetItem, QHBoxLayout, QVBoxLayout, \
    QAbstractItemView
from from_root import from_root

from model.Materials import Material
from model.Tradepost import TradePost
from service.GameCoordinatorService import GameCoordinatorService
from ui_components.MainViewABC import MainViewABC


class QListWidgetBuyOfferItem(QListWidgetItem):
    def __init__(self, text: str, offer_data: dict):
        super().__init__(text)
        self._offer_data: dict = offer_data

    @property
    def offer(self) -> dict:
        return self._offer_data


class QListWidgetSellOfferItem(QListWidgetItem):
    def __init__(self, text: str, material: Type[Material]):
        super().__init__(text)
        self._material: Type[Material] = material

    @property
    def material(self) -> Type[Material]:
        return self._material


class TradePostView(MainViewABC):
    def __init__(self, game_coordinator_service: GameCoordinatorService):
        super().__init__()
        self.setFixedSize(1280, 660)
        self._game_coordinator_service: GameCoordinatorService = game_coordinator_service
        self._error_timer = QTimer()

        # Load styles
        with open(from_root("style", "TradePostView.qss"), "r") as style_file:
            self._style_sheet = style_file.read()

        self._background_label = QLabel("", self)
        self._background_label.setObjectName("TradePostViewBackground")
        self._background_label.setFixedSize(self.width(), self.height())

        # Layouts
        self._main_widget = QWidget(self)
        self._main_widget.setFixedSize(self.width(), self.height())
        self._layout = QHBoxLayout()
        self._layout.setContentsMargins(10, 10, 10, 10)
        self._left_layout = QVBoxLayout()
        self._left_layout.setContentsMargins(0, 0, 0, 0)
        self._left_layout.setSpacing(0)
        self._middle_layout = QVBoxLayout()
        self._right_layout = QVBoxLayout()
        self._right_layout.setContentsMargins(0, 0, 0, 0)
        self._right_layout.setSpacing(0)
        self._layout.addLayout(self._left_layout)
        self._layout.addLayout(self._middle_layout)
        self._layout.addLayout(self._right_layout)
        self._main_widget.setLayout(self._layout)

        self._trade_post_info_label = QLabel("")
        self._trade_post_info_label.setObjectName("TradePostSideViewInfoLabel")
        self._trade_post_list_label = QLabel(" -COST-        -MATERIAL-          -STOCK-")
        self._trade_post_list_label.setObjectName("TradePostSideViewListLabel")
        self._trade_post_list = QListWidget()
        self._trade_post_list.setObjectName("TradePostSideViewList")
        self._trade_post_list.setSelectionMode(QAbstractItemView.SelectionMode.SingleSelection)
        self._left_layout.addWidget(self._trade_post_info_label)
        self._left_layout.addWidget(self._trade_post_list_label)
        self._left_layout.addWidget(self._trade_post_list)

        label_row_1 = QVBoxLayout()
        button_row_1 = QHBoxLayout()
        button_row_2 = QHBoxLayout()
        label_row_2 = QVBoxLayout()
        button_row_3 = QHBoxLayout()
        button_row_4 = QHBoxLayout()

        self._buy_label = QLabel("Buy")
        self._buy_label.setObjectName("TradePostSideViewBuyLabel")
        label_row_1.addWidget(self._buy_label)
        self._middle_layout.addLayout(label_row_1)

        self._buy_one_button = QPushButton(" x1")
        self._buy_one_button.setObjectName("TradePostSideViewBuyOne")
        self._buy_one_button.clicked.connect(lambda: self._buy_selected(1))
        self._buy_ten_button = QPushButton("x10")
        self._buy_ten_button.setObjectName("TradePostSideViewBuyTen")
        self._buy_ten_button.clicked.connect(lambda: self._buy_selected(10))
        button_row_1.addWidget(self._buy_one_button)
        button_row_1.addWidget(self._buy_ten_button)
        self._middle_layout.addLayout(button_row_1)

        self._buy_hundred_button = QPushButton("x100")
        self._buy_hundred_button.setObjectName("TradePostSideViewBuyHundred")
        self._buy_hundred_button.clicked.connect(lambda: self._buy_selected(100))
        self._buy_all_button = QPushButton("ALL")
        self._buy_all_button.setObjectName("TradePostSideViewBuyAll")
        self._buy_all_button.clicked.connect(lambda: self._buy_selected(-1))
        button_row_2.addWidget(self._buy_hundred_button)
        button_row_2.addWidget(self._buy_all_button)
        self._middle_layout.addLayout(button_row_2)

        self._sell_label = QLabel("Sell")
        self._sell_label.setObjectName("CargoBaySideViewSellLabel")
        label_row_2.addSpacing(140)
        label_row_2.addWidget(self._sell_label)
        self._middle_layout.addLayout(label_row_2)

        self._sell_one_button = QPushButton(" x1")
        self._sell_one_button.setObjectName("CargoBaySideViewSellOne")
        self._sell_one_button.clicked.connect(lambda: self._sell_selected(1))
        self._sell_ten_button = QPushButton("x10")
        self._sell_ten_button.setObjectName("CargoBaySideViewSellTen")
        self._sell_ten_button.clicked.connect(lambda: self._sell_selected(10))
        button_row_3.addWidget(self._sell_one_button)
        button_row_3.addWidget(self._sell_ten_button)
        self._middle_layout.addLayout(button_row_3)

        self._sell_hundred_button = QPushButton("x100")
        self._sell_hundred_button.setObjectName("CargoBaySideViewSellHundred")
        self._sell_hundred_button.clicked.connect(lambda: self._sell_selected(100))
        self._sell_all_button = QPushButton("ALL")
        self._sell_all_button.setObjectName("CargoBaySideViewSellAll")
        self._sell_all_button.clicked.connect(lambda: self._sell_selected(-1))
        button_row_4.addWidget(self._sell_hundred_button)
        button_row_4.addWidget(self._sell_all_button)
        self._middle_layout.addLayout(button_row_4)

        self._cargo_bay_info_label = QLabel(f"Credits: {str(self._game_coordinator_service.session.player.credits)}\n"
                                            f"Cargo Capacity: {self._game_coordinator_service.session.player.ship.inventory.current_cargo_capacity}"
                                            f"/{self._game_coordinator_service.session.player.ship.inventory.max_cargo_capacity}")
        self._cargo_bay_info_label.setWordWrap(True)
        self._cargo_bay_info_label.setObjectName("CargoBaySideViewInfoLabel")
        self._cargo_bay_list_label = QLabel(" -PRICE-     -MATERIAL-      -STOCK-",)
        self._cargo_bay_list_label.setObjectName("CargoBaySideViewListLabel")
        self._cargo_bay_list = QListWidget()
        self._cargo_bay_list.setObjectName("CargoBaySideViewList")
        self._cargo_bay_list.setSelectionMode(QAbstractItemView.SelectionMode.SingleSelection)
        self._right_layout.addWidget(self._cargo_bay_info_label)
        self._right_layout.addWidget(self._cargo_bay_list_label)
        self._right_layout.addWidget(self._cargo_bay_list)

        self._update_trade_post_list()
        self._update_cargo_bay_list()
        self.setStyleSheet(self._style_sheet)

    def _buy_selected(self, amount: int) -> None:
        try:
            selected_item = self._trade_post_list.selectedItems()[0]
            offer = selected_item.offer
            if amount == -1:
                amount = offer["in_stock"]
        except IndexError:  # No item selected
            return

        logging.info(f"Buying {amount} {offer['material'].name} at {offer['buy_price']} a piece. Total Stock: {offer['in_stock']}")

        error_msg: Union[str, None] = None
        if amount > offer['in_stock']:  # Can not sell more than is in stock
            error_msg = "Not enough stock"
        elif offer['buy_price'] * amount > self._game_coordinator_service.session.player.credits:  # Not enough credits
            error_msg = "Not enough credits"
        elif (self._game_coordinator_service.session.player.ship.inventory.current_cargo_capacity + amount) > \
                self._game_coordinator_service.session.player.ship.inventory.max_cargo_capacity:  # Cargo bay full
            error_msg = "Not enough capacity in cargo bay"
        if error_msg:
            logging.debug(error_msg)
            self._display_error_message(error_msg)
            return

        trade_post = self._game_coordinator_service.session.player.planet_location.trade_post
        price = trade_post.get_total_price(type(offer['material']), amount)
        materials_or_error = trade_post.buy(type(offer['material']), amount)
        if not isinstance(materials_or_error, Material):  # This should never occur, but just in case.
            logging.debug(f"Error {materials_or_error} occurred during transaction.")
            return
        material = materials_or_error
        self._game_coordinator_service.session.player.remove_credits(price)
        self._game_coordinator_service.session.player.ship.inventory.add_material(material)
        self._update_trade_post_list()
        self._update_cargo_bay_list()
        self._update_credits_label()

    def _sell_selected(self, amount: int) -> None:
        ship_inventory = self._game_coordinator_service.session.player.ship.inventory
        trade_post = self._game_coordinator_service.session.player.planet_location.trade_post
        try:
            selected_item = self._cargo_bay_list.selectedItems()[0]
            material_to_be_sold = list(filter(
                lambda m: isinstance(m, selected_item.material), ship_inventory.get_inventory_list()
            ))[0]
            if amount == -1:
                amount = material_to_be_sold.quantity
        except IndexError:  # No item selected
            return

        sell_price = trade_post.get_selling_price(type(material_to_be_sold))
        logging.info(f"Selling {amount} {material_to_be_sold.name} for {sell_price} a piece. Total Stock: {material_to_be_sold.quantity}")

        error_msg: Union[str, None] = None
        if amount > material_to_be_sold.quantity:
            error_msg = "Not enough stock on ship"
        if sell_price == TradePost.NOT_FOR_SALE:
            error_msg = "TradePost does not buy this material"
        if error_msg:
            logging.debug(error_msg)
            self._display_error_message(error_msg)
            return

        ship_inventory.remove_material(type(material_to_be_sold), amount)
        self._game_coordinator_service.session.player.add_credits(
            trade_post.sell(type(material_to_be_sold)(amount))
        )
        self._update_trade_post_list()
        self._update_cargo_bay_list()
        self._update_credits_label()

    def _update_credits_label(self) -> None:
        self._cargo_bay_info_label.setText(f"Credits: {str(self._game_coordinator_service.session.player.credits)}\n"
                                           f"Cargo Capacity: {self._game_coordinator_service.session.player.ship.inventory.current_cargo_capacity}"
                                           f"/{self._game_coordinator_service.session.player.ship.inventory.max_cargo_capacity}")

    def _update_trade_post_list(self) -> None:
        try:
            selected_item_type = type(self._trade_post_list.selectedItems()[0].offer["material"])
        except IndexError:
            selected_item_type = None
        trade_post = self._game_coordinator_service.session.player.planet_location.trade_post

        self._trade_post_list.clear()
        for offer in trade_post.offer_table:
            if offer['in_stock'] < 1:
                continue
            item = QListWidgetBuyOfferItem(f"[{str(offer['buy_price']) + ']':<9} {offer['material'].name:<30} [{str(offer['in_stock']) + ']':<4}", offer)
            item.setFont("Roboto Mono")
            self._trade_post_list.addItem(item)
        if selected_item_type:
            for item_idx in range(self._trade_post_list.count()):
                if type(self._trade_post_list.item(item_idx).offer["material"]) == selected_item_type:
                    self._trade_post_list.item(item_idx).setSelected(True)

    def _update_cargo_bay_list(self) -> None:
        try:
            selected_item_type = self._cargo_bay_list.selectedItems()[0].material
        except IndexError:
            selected_item_type = None
        trade_post = self._game_coordinator_service.session.player.planet_location.trade_post
        cargo_bay = self._game_coordinator_service.session.player.ship.inventory.get_inventory_list()
        self._cargo_bay_list.clear()
        for material in cargo_bay:
            price = trade_post.get_selling_price(type(material))
            if price < 1:
                price = 0
            item = QListWidgetSellOfferItem(f"[{str(price) + ']':<9} {material.name:<23} [{str(material.quantity) + ']':<4}", type(material))
            item.setFont("Roboto Mono")
            self._cargo_bay_list.addItem(item)
        if selected_item_type:
            for item_idx in range(self._cargo_bay_list.count()):
                if self._cargo_bay_list.item(item_idx).material == selected_item_type:
                    self._cargo_bay_list.item(item_idx).setSelected(True)

    def _display_error_message(self, error_message: str) -> None:
        self._trade_post_info_label.setText(error_message)
        self._error_timer.timeout.connect(lambda: self._trade_post_info_label.setText(""))
        self._error_timer.setSingleShot(True)
        self._error_timer.start(2000)
